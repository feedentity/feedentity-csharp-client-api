/* 
 * FdeIntegrations API
 *
 * FdeIntegrations API documentation
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using clientapi.Api;
using clientapi.Model;
using clientapi.Client;
using System.Reflection;

namespace clientapi.Test
{
    /// <summary>
    ///  Class for testing LotRaisingDTO
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class LotRaisingDTOTests
    {
        // TODO uncomment below to declare an instance variable for LotRaisingDTO
        //private LotRaisingDTO instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of LotRaisingDTO
            //instance = new LotRaisingDTO();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of LotRaisingDTO
        /// </summary>
        [Test]
        public void LotRaisingDTOInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" LotRaisingDTO
            //Assert.IsInstanceOfType<LotRaisingDTO> (instance, "variable 'instance' is a LotRaisingDTO");
        }

        /// <summary>
        /// Test the property 'EndingDate'
        /// </summary>
        [Test]
        public void EndingDateTest()
        {
            // TODO unit test for the property 'EndingDate'
        }
        /// <summary>
        /// Test the property 'Id'
        /// </summary>
        [Test]
        public void IdTest()
        {
            // TODO unit test for the property 'Id'
        }
        /// <summary>
        /// Test the property 'StartingDate'
        /// </summary>
        [Test]
        public void StartingDateTest()
        {
            // TODO unit test for the property 'StartingDate'
        }

    }

}
