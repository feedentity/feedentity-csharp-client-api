# clientapi.Api.FeedentityOrganizationsApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetAllOrganizationByAuthKeyUsingGET**](FeedentityOrganizationsApi.md#getallorganizationbyauthkeyusingget) | **GET** /api/v1/orgs/{authKey}/pus | Finds organization by auth key


<a name="getallorganizationbyauthkeyusingget"></a>
# **GetAllOrganizationByAuthKeyUsingGET**
> List<OrganizationDTO> GetAllOrganizationByAuthKeyUsingGET (string authKey)

Finds organization by auth key

List of all organization defined by auth key with production units

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class GetAllOrganizationByAuthKeyUsingGETExample
    {
        public void main()
        {
            var apiInstance = new FeedentityOrganizationsApi();
            var authKey = authKey_example;  // string | authKey

            try
            {
                // Finds organization by auth key
                List&lt;OrganizationDTO&gt; result = apiInstance.GetAllOrganizationByAuthKeyUsingGET(authKey);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FeedentityOrganizationsApi.GetAllOrganizationByAuthKeyUsingGET: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authKey** | **string**| authKey | 

### Return type

[**List<OrganizationDTO>**](OrganizationDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

