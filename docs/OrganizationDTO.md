# clientapi.Model.OrganizationDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** |  | [optional] 
**Name** | **string** |  | [optional] 
**ProductionUnits** | [**List&lt;OrganizationPuDTO&gt;**](OrganizationPuDTO.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

