# clientapi.Model.LotPuDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** |  | [optional] 
**OrganizationPuDTO** | [**OrganizationPuDTO**](OrganizationPuDTO.md) |  | [optional] 
**Size** | **double?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

