# clientapi.Model.OrganizationPuPartDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CatastalPart** | **string** |  | [optional] 
**CatastalSheet** | **string** |  | [optional] 
**Id** | **long?** |  | [optional] 
**Length** | **double?** |  | [optional] 
**Sections** | **int?** |  | [optional] 
**Size** | **double?** |  | [optional] 
**Width** | **double?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

