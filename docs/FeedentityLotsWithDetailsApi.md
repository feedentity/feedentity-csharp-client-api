# clientapi.Api.FeedentityLotsWithDetailsApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetLotDetailsUsingGET**](FeedentityLotsWithDetailsApi.md#getlotdetailsusingget) | **GET** /api/v1/orgs/{orgId}/lot/{lotId} | Finds a single lot with all details
[**GetOrganizationLotForPuUsingGET**](FeedentityLotsWithDetailsApi.md#getorganizationlotforpuusingget) | **GET** /api/v1/orgs/{orgId}/pus/{puId} | Finds all lots by organization and production unit


<a name="getlotdetailsusingget"></a>
# **GetLotDetailsUsingGET**
> LotDTO GetLotDetailsUsingGET (long? orgId, string lotId)

Finds a single lot with all details

Single lot object with all informations about production unit associated and list of raisings, plant protections, fertilizations and irrigations.

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class GetLotDetailsUsingGETExample
    {
        public void main()
        {
            var apiInstance = new FeedentityLotsWithDetailsApi();
            var orgId = 789;  // long? | orgId
            var lotId = lotId_example;  // string | lotId

            try
            {
                // Finds a single lot with all details
                LotDTO result = apiInstance.GetLotDetailsUsingGET(orgId, lotId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FeedentityLotsWithDetailsApi.GetLotDetailsUsingGET: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgId** | **long?**| orgId | 
 **lotId** | **string**| lotId | 

### Return type

[**LotDTO**](LotDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getorganizationlotforpuusingget"></a>
# **GetOrganizationLotForPuUsingGET**
> LotForPuDTO GetOrganizationLotForPuUsingGET (long? orgId, long? puId)

Finds all lots by organization and production unit

List of all lot for an organization and production unit

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class GetOrganizationLotForPuUsingGETExample
    {
        public void main()
        {
            var apiInstance = new FeedentityLotsWithDetailsApi();
            var orgId = 789;  // long? | orgId
            var puId = 789;  // long? | puId

            try
            {
                // Finds all lots by organization and production unit
                LotForPuDTO result = apiInstance.GetOrganizationLotForPuUsingGET(orgId, puId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FeedentityLotsWithDetailsApi.GetOrganizationLotForPuUsingGET: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **orgId** | **long?**| orgId | 
 **puId** | **long?**| puId | 

### Return type

[**LotForPuDTO**](LotForPuDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

