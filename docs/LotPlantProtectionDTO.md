# clientapi.Model.LotPlantProtectionDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AdversityId** | **long?** |  | [optional] 
**AdversityName** | **string** |  | [optional] 
**AdversityScientificName** | **string** |  | [optional] 
**AppMethod** | **string** |  | [optional] 
**Date** | **DateTime?** |  | [optional] 
**FailureTime** | **int?** |  | [optional] 
**Id** | **long?** |  | [optional] 
**MixDistribuited** | **double?** |  | [optional] 
**OperatorFullName** | **string** |  | [optional] 
**OperatorId** | **long?** |  | [optional] 
**PhenologicalPhase** | **string** |  | [optional] 
**ProductId** | **long?** |  | [optional] 
**ProductName** | **string** |  | [optional] 
**Quantity** | **double?** |  | [optional] 
**ReturnPeriod** | **int?** |  | [optional] 
**Start** | [**LocalTime**](LocalTime.md) |  | [optional] 
**Stop** | [**LocalTime**](LocalTime.md) |  | [optional] 
**Udm** | **string** |  | [optional] 
**Weather** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

