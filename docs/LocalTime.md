# clientapi.Model.LocalTime
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Hour** | **int?** |  | [optional] 
**Minute** | **int?** |  | [optional] 
**Nano** | **int?** |  | [optional] 
**Second** | **int?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

