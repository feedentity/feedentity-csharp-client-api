# clientapi.Model.LotFertilizerDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AppMethod** | **string** |  | [optional] 
**B** | **double?** |  | [optional] 
**Ca** | **double?** |  | [optional] 
**Cu** | **double?** |  | [optional] 
**Date** | **DateTime?** |  | [optional] 
**Density** | **double?** |  | [optional] 
**Fe** | **double?** |  | [optional] 
**Id** | **long?** |  | [optional] 
**K** | **double?** |  | [optional] 
**Mg** | **double?** |  | [optional] 
**MixDistribuited** | **double?** |  | [optional] 
**Mn** | **double?** |  | [optional] 
**Mo** | **double?** |  | [optional] 
**N** | **double?** |  | [optional] 
**OperatorFullName** | **string** |  | [optional] 
**OperatorId** | **long?** |  | [optional] 
**P** | **double?** |  | [optional] 
**Ph** | **double?** |  | [optional] 
**PhenologicalPhase** | **string** |  | [optional] 
**Product** | **string** |  | [optional] 
**Quantity** | **double?** |  | [optional] 
**S** | **double?** |  | [optional] 
**Start** | [**LocalTime**](LocalTime.md) |  | [optional] 
**Stop** | [**LocalTime**](LocalTime.md) |  | [optional] 
**Udm** | **string** |  | [optional] 
**Weather** | **string** |  | [optional] 
**Zn** | **double?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

