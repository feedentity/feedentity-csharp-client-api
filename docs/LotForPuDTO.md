# clientapi.Model.LotForPuDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Lots** | [**List&lt;LotDTO&gt;**](LotDTO.md) |  | [optional] 
**OrganizationId** | **long?** |  | [optional] 
**OrganizationName** | **string** |  | [optional] 
**ProductionUnitDimension** | **double?** |  | [optional] 
**ProductionUnitId** | **long?** |  | [optional] 
**ProductionUnitName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

