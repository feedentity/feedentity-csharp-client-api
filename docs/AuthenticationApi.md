# clientapi.Api.AuthenticationApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AuthorizeUsingPOST**](AuthenticationApi.md#authorizeusingpost) | **POST** /api/authenticate | Get Authentication Token


<a name="authorizeusingpost"></a>
# **AuthorizeUsingPOST**
> JWTToken AuthorizeUsingPOST (LoginVM loginVM)

Get Authentication Token

Get the JWT token for authentication

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class AuthorizeUsingPOSTExample
    {
        public void main()
        {
            var apiInstance = new AuthenticationApi();
            var loginVM = new LoginVM(); // LoginVM | loginVM

            try
            {
                // Get Authentication Token
                JWTToken result = apiInstance.AuthorizeUsingPOST(loginVM);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AuthenticationApi.AuthorizeUsingPOST: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loginVM** | [**LoginVM**](LoginVM.md)| loginVM | 

### Return type

[**JWTToken**](JWTToken.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

