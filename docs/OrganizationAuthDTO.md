# clientapi.Model.OrganizationAuthDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Activated** | **bool?** |  | 
**AuthKey** | **string** |  | 
**Id** | **long?** |  | [optional] 
**Note** | **string** |  | [optional] 
**OrganizationId** | **long?** |  | 
**UserId** | **long?** |  | [optional] 
**UserLogin** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

