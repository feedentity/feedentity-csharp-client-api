# clientapi.Model.LotRaisingDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**EndingDate** | **DateTime?** |  | [optional] 
**Id** | **long?** |  | [optional] 
**StartingDate** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

