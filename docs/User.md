# clientapi.Model.User
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Activated** | **bool?** |  | 
**Email** | **string** |  | [optional] 
**FirstName** | **string** |  | [optional] 
**Id** | **long?** |  | [optional] 
**ImageUrl** | **string** |  | [optional] 
**LangKey** | **string** |  | [optional] 
**LastName** | **string** |  | [optional] 
**Login** | **string** |  | 
**ResetDate** | **DateTime?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

