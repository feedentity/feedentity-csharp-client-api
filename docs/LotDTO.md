# clientapi.Model.LotDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ClosureDate** | **DateTime?** |  | [optional] 
**Code** | **string** |  | [optional] 
**CropId** | **long?** |  | [optional] 
**CropName** | **string** |  | [optional] 
**CropScientificName** | **string** |  | [optional] 
**CropType** | **string** |  | [optional] 
**CropVarietyId** | **long?** |  | [optional] 
**CropVarietyName** | **string** |  | [optional] 
**Fertilizers** | [**List&lt;LotFertilizerDTO&gt;**](LotFertilizerDTO.md) |  | [optional] 
**Gage** | **string** |  | [optional] 
**Id** | **string** |  | [optional] 
**Irrigations** | [**List&lt;LotIrrigationDTO&gt;**](LotIrrigationDTO.md) |  | [optional] 
**LotType** | **string** |  | [optional] 
**PlantProtections** | [**List&lt;LotPlantProtectionDTO&gt;**](LotPlantProtectionDTO.md) |  | [optional] 
**ProductionUnits** | [**List&lt;OrganizationPuDTO&gt;**](OrganizationPuDTO.md) |  | [optional] 
**Raisings** | [**List&lt;LotRaisingDTO&gt;**](LotRaisingDTO.md) |  | [optional] 
**SeedTreatment** | **string** |  | [optional] 
**SeedlingPassport** | **string** |  | [optional] 
**Size** | **double?** |  | [optional] 
**SowingDate** | **DateTime?** |  | [optional] 
**SowingDensity** | **double?** |  | [optional] 
**TotalSeeds** | **double?** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

