# clientapi.Model.LotIrrigationDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AppMethod** | **string** |  | [optional] 
**Date** | **DateTime?** |  | [optional] 
**DistributedQty** | **double?** |  | [optional] 
**Id** | **long?** |  | [optional] 
**IrrigationSystemId** | **long?** |  | [optional] 
**IrrigationSystemName** | **string** |  | [optional] 
**IrrigatorFlow** | **double?** |  | [optional] 
**IrrigatorsCount** | **int?** |  | [optional] 
**JetDistance** | **double?** |  | [optional] 
**Length** | **double?** |  | [optional] 
**Lines** | **int?** |  | [optional] 
**LotPuDTO** | [**LotPuDTO**](LotPuDTO.md) |  | [optional] 
**OperatorFullName** | **string** |  | [optional] 
**OperatorId** | **long?** |  | [optional] 
**PhenologicalPhase** | **string** |  | [optional] 
**Start** | [**LocalTime**](LocalTime.md) |  | [optional] 
**Stop** | [**LocalTime**](LocalTime.md) |  | [optional] 
**Weather** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

