# clientapi.Api.AccountAndAutenticationApi

All URIs are relative to *https://18.194.178.129:8080/fde-integrations*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ChangePasswordUsingPOST**](AccountAndAutenticationApi.md#changepasswordusingpost) | **POST** /api/account/change-password | Change password
[**GetAccountUsingGET**](AccountAndAutenticationApi.md#getaccountusingget) | **GET** /api/account | Get account informations
[**IsAuthenticatedUsingGET**](AccountAndAutenticationApi.md#isauthenticatedusingget) | **GET** /api/authenticate | Verify if user is authenticate
[**SaveAccountUsingPOST**](AccountAndAutenticationApi.md#saveaccountusingpost) | **POST** /api/account | Update account informations


<a name="changepasswordusingpost"></a>
# **ChangePasswordUsingPOST**
> void ChangePasswordUsingPOST (string password)

Change password

To change account passord

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class ChangePasswordUsingPOSTExample
    {
        public void main()
        {
            var apiInstance = new AccountAndAutenticationApi();
            var password = password_example;  // string | password

            try
            {
                // Change password
                apiInstance.ChangePasswordUsingPOST(password);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountAndAutenticationApi.ChangePasswordUsingPOST: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **password** | **string**| password | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getaccountusingget"></a>
# **GetAccountUsingGET**
> UserDTO GetAccountUsingGET ()

Get account informations

Return an object with current user informations

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class GetAccountUsingGETExample
    {
        public void main()
        {
            var apiInstance = new AccountAndAutenticationApi();

            try
            {
                // Get account informations
                UserDTO result = apiInstance.GetAccountUsingGET();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountAndAutenticationApi.GetAccountUsingGET: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UserDTO**](UserDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="isauthenticatedusingget"></a>
# **IsAuthenticatedUsingGET**
> string IsAuthenticatedUsingGET ()

Verify if user is authenticate

If user is authenticate returns 200

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class IsAuthenticatedUsingGETExample
    {
        public void main()
        {
            var apiInstance = new AccountAndAutenticationApi();

            try
            {
                // Verify if user is authenticate
                string result = apiInstance.IsAuthenticatedUsingGET();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountAndAutenticationApi.IsAuthenticatedUsingGET: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="saveaccountusingpost"></a>
# **SaveAccountUsingPOST**
> void SaveAccountUsingPOST (UserDTO userDTO)

Update account informations

To save user account informations

### Example
```csharp
using System;
using System.Diagnostics;
using clientapi.Api;
using clientapi.Client;
using clientapi.Model;

namespace Example
{
    public class SaveAccountUsingPOSTExample
    {
        public void main()
        {
            var apiInstance = new AccountAndAutenticationApi();
            var userDTO = new UserDTO(); // UserDTO | userDTO

            try
            {
                // Update account informations
                apiInstance.SaveAccountUsingPOST(userDTO);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AccountAndAutenticationApi.SaveAccountUsingPOST: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userDTO** | [**UserDTO**](UserDTO.md)| userDTO | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

