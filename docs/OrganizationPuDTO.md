# clientapi.Model.OrganizationPuDTO
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Dimension** | **double?** |  | [optional] 
**Id** | **long?** |  | [optional] 
**Name** | **string** |  | [optional] 
**ProductionUnitParts** | [**List&lt;OrganizationPuPartDTO&gt;**](OrganizationPuPartDTO.md) |  | [optional] 
**Type** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

